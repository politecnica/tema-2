#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void detectarsenhal();

int main(void){
    /* iniciaización de variables*/
    int status = 0;
    int cantidadProcesos = 0;
    int signal, delay;
    FILE *ptrArchivo;
    pid_t child = 1;

    /*abre el archivo donde va a ser escrito los PID de los procesos*/
    ptrArchivo = fopen("datos.txt", "w+");

    printf("Cantidad de procesos a ser creados:\n");
    scanf("%d", &cantidadProcesos);

    for(int i = 0; i < cantidadProcesos; i++){
        child = fork();
        if(child == 0){
            detectarsenhal();
            exit(0);
        }
        else if(child < 0){
            perror("Error en el fork");
            exit(-1);
        }
        else{
            printf("Proceso numero %d, ingrese la senhal y el delay\n", i + 1);
            scanf("%d", &signal);
            scanf("%d", &delay);
            fprintf(ptrArchivo, "%d\t%d\t%d\n", child, signal, delay);
        }
    }
    fclose(ptrArchivo);
    child = fork();
    if(child == 0){
        char *args[]={"./enviarSignal",NULL};
        execv(args[0],args);
        exit(0);
    }
    else if(child < 0){
        perror("Error en el fork");
        exit(-1);
    }

    while (( wait(&status)) > 0);
    return 0;
}
void detectarsenhal(){
    //conjunto de todas las señales
    sigset_t mySet;

    //se llena con todas las señales posibles
    sigfillset(&mySet);
    //se asigna a la mascara y se bloquea todas las señales
    sigprocmask(SIG_BLOCK, &mySet, NULL);
    //Nbr de la señal
    int signalNbr;
    //espera de la señal
    sigwait(&mySet, &signalNbr);
    //si hubo un error
    if (signalNbr == -1) {
        perror("Failed to wait using sigwait");
        return;
    } // End if
    else{
        fprintf(stdout, "se recibio la signal #%d\n", signalNbr);
    }
}
